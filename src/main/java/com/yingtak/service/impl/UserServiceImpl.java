package com.yingtak.service.impl;

import com.yingtak.dao.IUserDao;
import com.yingtak.model.User;
import com.yingtak.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("userService")
public class UserServiceImpl implements IUserService {

    @Resource
    private IUserDao userDao;

    public User selectUser(int userId){
        return this.userDao.selectUser(userId);
    }
}

package com.yingtak.service;

import com.yingtak.model.User;

public interface IUserService {
    User selectUser(int userId);
}

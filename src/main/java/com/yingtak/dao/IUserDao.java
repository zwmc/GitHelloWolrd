package com.yingtak.dao;

import com.yingtak.model.User;

public interface IUserDao {

    User selectUser(int id);
}
